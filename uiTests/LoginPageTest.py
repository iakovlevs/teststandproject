from selenium import webdriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException


def wait_until_clickable(driver, by, value, timeout=10):
   return WebDriverWait(driver, timeout).until(ec.element_to_be_clickable((by, value)))


def wait_until_visible(driver, by, value, timeout=10):
   return WebDriverWait(driver, timeout).until(ec.visibility_of_element_located((by, value)))


def check_element_present(driver, by, value, timeout=3):
   try:
       return wait_until_visible(driver, by, value, timeout)
   except TimeoutException:
       return False


def test_wrong_login():
   driver = webdriver.Chrome('/Users/s.yakovlev/Downloads/chromedriver')
   driver.get("https://test-stand.gb.ru/login")
   wait_until_clickable(driver, By.XPATH, '//*[@id="login"]/div[1]/label/input').send_keys("wrongpass")
   wait_until_clickable(driver, By.XPATH, '//*[@id="login"]/div[2]/label/input').send_keys("wrongpass")
   wait_until_clickable(driver, By.XPATH, '//*[@id="login"]/div[3]/button/span').click()
   assert check_element_present(driver, By.XPATH, '//*[@id="app"]/main/div/div/div[2]/p[1]')
   driver.quit()

def test_correct_login():
   driver = webdriver.Chrome('/Users/s.yakovlev/Downloads/chromedriver')
   driver.get("https://test-stand.gb.ru/login")
   wait_until_clickable(driver, By.XPATH, '//*[@id="login"]/div[1]/label/input').send_keys("GB2022105f8e82")
   wait_until_clickable(driver, By.XPATH, '//*[@id="login"]/div[2]/label/input').send_keys("39aecbd168")
   wait_until_clickable(driver, By.XPATH, '//*[@id="login"]/div[3]/button/span').click()
   assert check_element_present(driver, By.XPATH, '//*[@id="app"]/main/nav/ul/li[3]/a')
   driver.quit()

